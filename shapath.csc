class sdfdss {

// useful for 
// a)showing title in set of cmd prompt windows
// b)as well as making long prompts irrelevant and letting you lessen the width of the cmd window.. rather than widening it to view the path.

// the 2  subdirectory directory is useful if e.g. the project name is   Projects\showabbreviatedpath (because it's not obvious that it's a project or vs project)
// the 1 subdirectory directory is useful for e.g. if the project name is  kwicanalyze

  public static void Main(string[] args) {
            //http://stackoverflow.com/questions/15653921/get-current-folder-path

            string s = System.IO.Directory.GetCurrentDirectory();

            string[] dirs= s.Split('\\'); // 'ab' fails. so we know '\\' is escaping fine.

           System.Console.Write("run: title ");
            for (int i = dirs.Length-2; i <= dirs.Length-1; i++)
                System.Console.Write(dirs[i] + "\\");

            System.Console.WriteLine();
            System.Console.WriteLine("or run: title "+dirs[dirs.Length-1]+"\\");
 
            // //set environment variable, <-- no benefit.
           //set title
            //prompt may as well still be $p$_$g   width of cmd made much less.. 
            // prompt is a bit useless..  cos $p$g is too long cursor ends up too far across
             // and..  if puttinga variable with it it is not updated.

                       
            
   }
}